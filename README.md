# A simple sauce list manager

Simple sauce pun intended :)

A tiny Angular app composed of custom directives (tags and attributes) 
demonstrating customization of behavior and configuration to build a 
fully independent component:

** <sauce-app> main component tag **

	Displays the app and its components.

** <sauce-list> tag **

	Displays a list of "sauces" stored in LocalStorage, initiating a few on the first load.

** <add-sauce-form> tag **

	Provides a form with an "add-using" attribute for a custom handler.
	
** <remove-sauce-inline> tag **

	Provides an [x] link and a "remove-using" attribute for a custom handler.
	
** "sauce-click" attribute **

	Listens for clicks on children matched by "click-class" attribute and broadcast a "sauce:clicked" event.

# Quick start

1. npm install 
2. npm run-script view

## Online demo

[http://bitnbyte.bitbucket.org/angular-sauce-app](http://bitnbyte.bitbucket.org/angular-sauce-app)
