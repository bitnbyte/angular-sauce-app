angular.module('sauceApp', [
  'sauceList'
]).

directive('sauceApp', function() {

  return {

    scope: {},
    template: [

      '<div class="sauce-app">',
        '<h1>Sauces</h1>',
        '<sauce-list></sauce-list>',
      '</div>'

    ].join(''),

    replace: true,
    controller: 'AppCtrl',
    controllerAs: 'ctrl'

  };

}).

controller('AppCtrl', function($scope) {

});
