
angular.module('sauceList', [
  'addSauceForm',
  'removeSauceInline',
  'sauceClick'
]).

factory('Sauces', function() {

  if (null === localStorage.getItem('sauces')) {
    localStorage.setItem(
      'sauces',
      JSON.stringify([
        { name: 'Pesto' },
        { name: 'Chimichurri' },
        { name: 'Alfredo' }
      ])
    );

  }

  return {

    query: function() {

      return JSON.parse(
        localStorage.getItem('sauces')
      );

    },

    save: function(sauces) {

      localStorage.setItem(
        'sauces',
        JSON.stringify(sauces)
      );

    }

  };

}).

factory('SauceList', function() {

  var add = function(item) {

    if (!item || !item.name) {
      return;
    }

    item.name = item.name.trim();

    if (item.name) {
      this.items.unshift(item);
    }

  };

  var remove = function(item) {

    var index;

    if (!item || !item.name) {
      return;
    }

    index = this.items.indexOf(item);

    if (index > -1) {
      return this.items.splice(index, 1);
    }

  };

  return {

    items: [],
    addToList: add,
    removeFromList: remove

  };

}).

directive('sauceList', function() {

  return {

    scope: {},
    template: [

      '<div class="sauce-list">',
      '<add-sauce-form add-using="ctrl.addSauce">',
      '</add-sauce-form>',
        '<sauce-clicked></sauce-clicked>',
        '<ul sauce-click click-class="sauce-clickable">',
          '<li ng-repeat="sauce in ctrl.sauces track by $id($index)">',
            '<a class="sauce-clickable" href="javascript:void(0)">{{ sauce.name }}</a> ',
            '<remove-sauce-inline',
              ' remove-using="ctrl.removeSauce" sauce="sauce">',
            '</remove-sauce-inline>',
          '</li>',
        '</ul>',
      '</div>'

    ].join(''),
    replace: true,
    controller: 'ListCtrl',
    controllerAs: 'ctrl'

  };

}).

controller('ListCtrl', [

  'Sauces',
  'SauceList',

  function(Sauces, SauceList) {

    Sauces.query().forEach(
      function(sauce, index, theArray) {

        SauceList.addToList(sauce);

      }
    );

    Object.defineProperty( this, 'sauces', {

      get: function() {
        return SauceList.items;
      }

    });

    this.addSauce = function(sauce) {

      SauceList.addToList(sauce);
      Sauces.save(SauceList.items);

    };

    this.removeSauce = function(sauce) {

      SauceList.removeFromList(sauce);
      Sauces.save(SauceList.items);

    };

  }

]);
