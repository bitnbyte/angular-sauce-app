angular.module('removeSauceInline', []).

directive('removeSauceInline', function() {

  return {

    scope: {

      removeMethod: '=removeUsing',
      sauce: '='

    },
    replace: true,
    template: [

      '<a href="javascript:void(0)" ng-click="removeMethod(sauce)">',
        '[x]',
      '</a>'

    ].join(''),
    controller: 'RemoveSauceInlineCtrl',
    controllerAs: 'ctrl'

  };

}).

controller('RemoveSauceInlineCtrl', function($scope) {

});
