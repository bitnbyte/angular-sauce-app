angular.module('sauceClick', []).

factory('Clicked', function() {

  return {
    item: 'none'
  };

}).

directive('sauceClick', [
  '$rootScope',
  'Clicked',
  function($rootScope, Clicked) {

  return {

    scope: {
      notify: '='
    },
    restrict: 'A',
    link: function(scope, elem, attr) {

      var clickClassRe;

      elem.on('click', function(e) {

        var eSrc = (e.srcElement || e.target);

        if (eSrc.hasAttribute('class')) {

          if ('clickClass' in attr) {

            clickClassRe = new RegExp(
              '\\b' + attr.clickClass
                .replace(/([^\\])\-/g, '$1\\-') + '\\b'
            );

          } else {

            clickClassRe = /sauce\-clickable/;

          }

          if (clickClassRe.
                test(eSrc.getAttribute('class'))) {

            Clicked.item = eSrc.textContent;

            $rootScope.$broadcast('sauce:clicked');
          }

        }

      });

    },
    replace: false

  };

}]).

directive('sauceClicked', function() {

  return {

    scope: {},
    template: '<p>Clicked: {{ ctrl.clicked }}</p>',
    replace: true,
    controller: 'SauceClickedCtrl',
    controllerAs: 'ctrl',

  };

}).

controller('SauceClickedCtrl', ['$scope', 'Clicked', function($scope, Clicked) {

  this.clicked = 'none';

  $scope.$on('sauce:clicked', angular.bind(this, function() {

    this.clicked = Clicked.item;
    $scope.$digest();

  }));

}]);
