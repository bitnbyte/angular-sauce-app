angular.module('addSauceForm', []).

directive('addSauceForm', function() {

  return {

    scope: {

      addMethod: '=addUsing'

    },
    template: [

      '<form>',
        '<h2>New Sauce</h2>',
        '<fieldset>',
          '<label>',
            'Sauce ',
            '<input ng-model="ctrl.sauce.name">',
          '</label>',
          '<button ng-click="ctrl.save()">',
            'Save',
          '</button>',
        '</fieldset>',
      '</form>'

    ].join(''),
    replace: true,
    controller: 'SauceFormCtrl',
    controllerAs: 'ctrl'

  };

}).

controller('SauceFormCtrl', function($scope) {

  this.sauce = {};

  this.save = function() {

    $scope.addMethod(this.sauce);
    this.sauce = {};

  }

});
